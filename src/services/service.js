export function AddItem(item) {
  const dataSource = JSON.parse(localStorage.getItem("linkData"));
  if (item && dataSource !== null) {
    dataSource.unshift(item);
    localStorage.setItem("linkData", JSON.stringify(dataSource));
    return { message: item.title.toUpperCase() + " Added.", result: "success" };
  }
}

export function DeleteItem(item) {
  const dataSource = JSON.parse(localStorage.getItem("linkData"));
  let deletedItem = dataSource.filter((e) => e.id !== item.id);
  localStorage.setItem("linkData", JSON.stringify(deletedItem));
  return JSON.parse(localStorage.getItem("linkData"));
}

export function VoteItem(item, type) {
  const dataSource = JSON.parse(localStorage.getItem("linkData"));
  let oldData = dataSource.filter((e) => e.id !== item.id);
  let pointItem = dataSource.filter((e) => e.id === item.id);
  if (type === "up") {
    pointItem[0].points++;
    oldData.push(pointItem[0]);
    let control = dataSource.filter((e) => e.points === pointItem[0].points);
    if (control.length > 1) {
      let sorting = oldData.sort(
        (a, b) => b.points - a.points || b.createdAt - a.createdAt
      );
      localStorage.setItem("linkData", JSON.stringify(sorting));
      return JSON.parse(localStorage.getItem("linkData"));
    } else {
      let sorting = oldData.sort((a, b) => b.points - a.points);
      localStorage.setItem("linkData", JSON.stringify(sorting));
      return JSON.parse(localStorage.getItem("linkData"));
    }
  } else if (type === "down") {
    pointItem[0].points--;
    oldData.push(pointItem[0]);
    let control = dataSource.filter((e) => e.points === pointItem[0].points);

    if (control.length > 1) {
      let sorting = oldData.sort(
        (a, b) => b.points - a.points || b.createdAt - a.createdAt
      );
      localStorage.setItem("linkData", JSON.stringify(sorting));
      return JSON.parse(localStorage.getItem("linkData"));
    } else {
      let sorting = oldData.sort((a, b) => b.points - a.points);
      localStorage.setItem("linkData", JSON.stringify(sorting));
      return JSON.parse(localStorage.getItem("linkData"));
    }
  }
}

export function SortByData(data, type) {
  if (type === "mostSort") {
    let mostFilter = data.sort((a, b) => b.points - a.points);
    localStorage.setItem("linkData", JSON.stringify(mostFilter));
    return JSON.parse(localStorage.getItem("linkData"));
  } else if (type === "lessSort") {
    let lessFilter = data.sort((a, b) => a.points - b.points);
    localStorage.setItem("linkData", JSON.stringify(lessFilter));
    return JSON.parse(localStorage.getItem("linkData"));
  }
}
