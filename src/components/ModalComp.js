import React from "react";
import CloseButton from "react-bootstrap/CloseButton";
import { DeleteItem } from "../services/service";
import Alert from "./Alert.js";
import { Modal } from "react-bootstrap";

function ModalComp({ selectedItem, isOpen, handleClose, getData }) {
  return (
    <Modal
      centered
      show={isOpen}
      onHide={handleClose}
      className="justify-content-center align-items-center text-center"
    >
      <Modal.Header className="modalHeader">
        <Modal.Title>Remove Link</Modal.Title>
        <Modal.Title>
          <CloseButton variant="white" onClick={handleClose} />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <span className="fw-lighter">Do you want to remove:</span>
        <h3 className="fw-bold">{selectedItem.title}</h3>
      </Modal.Body>
      <Modal.Footer
        style={{ border: "none" }}
        className="d-flex justify-content-around align-items-center "
      >
        <button
          className="subButton"
          onClick={() => {
            handleClose();
            getData(DeleteItem(selectedItem));
            Alert(selectedItem.title.toUpperCase() + " removed.");
          }}
        >
          OK
        </button>
        <button className="subButton" onClick={handleClose}>
          CANCEL
        </button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalComp;
