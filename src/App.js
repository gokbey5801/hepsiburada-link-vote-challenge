import React from "react";
import "./styles/App.css";
import { Routes, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Header } from "./objects/Header.js";

const AddLinkLazy = React.lazy(() => import("./pages/AddLink"));
const ListLazy = React.lazy(() => import("./pages/List"));
const NotFoundLazy = React.lazy(() => import("./pages/NotFound"));

let data = [
  {
    id: 1,
    title: "Hacker News",
    url: "( https://news.ycombinator.com/ )",
    points: 6,
    createdAt: 1647775313247,
  },
  {
    id: 2,
    title: "Product Hunt",
    url: "( https://producthunt.com/ )",
    points: 4,
    createdAt: 1647775313248,
  },
  {
    id: 3,
    title: "Quora",
    url: "( https://producthunt.com/ )",
    points: 3,
    createdAt: 1647775313249,
  },
];

if (localStorage.getItem("linkData") === null) {
  localStorage.setItem("linkData", JSON.stringify(data));
}

const loading = (
  <div style={{ marginTop: "20%" }} className="pt-3 text-center">
    <div className="display-1 spinner-border" role="status"></div>
  </div>
);

function App() {
  return (
    <div className="container mt-3 ">
      <React.Suspense fallback={loading}>
        <Header />
        <div className="row d-flex justify-content-center mt-4">
          <div className="mt-2 p-2 appContainer">
            <Routes>
              <Route exact path="/add-link" element={<AddLinkLazy />} />
              <Route exact path="/" element={<ListLazy />} />
              <Route path="*" element={<NotFoundLazy />} />
            </Routes>
            <ToastContainer />
          </div>
        </div>
      </React.Suspense>
    </div>
  );
}

export default App;
