### Hepsiburada Link Vote Challenge

### Links

- [🖥️ Website](https://hepsiburada-link-vote-challenge-gh.vercel.app/)

## Tasks

- CREATE PROJECT
- CREATE SERVICE
- CREATE COMPONENTS
- ~ Header
- ~ Card
- ~ Modal
- ~ Toast
- ~ Paginate
- PAGES
- ~ AddLink
- ~ List
- TESTİNG ( JEST/ ENZYME )

### Tools

- [React](https://reactjs.org)
- [Node](https://nodejs.org)
